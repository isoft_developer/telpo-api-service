package com.bluesky.telpo.api.dto;

import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class EmployeeAttendanceDTO {
    private List<LocalDateTime> localDateTimeFormatEmployeeAtt;
    private List<String> stringFormatEmployeeAtt;
    private String employeeNo;
}
