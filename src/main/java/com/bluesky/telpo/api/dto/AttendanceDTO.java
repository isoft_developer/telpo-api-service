package com.bluesky.telpo.api.dto;

import lombok.*;

import java.util.Date;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class AttendanceDTO {
    private Date attDate;
    private String attTime;
    private Long employeeId;
}
