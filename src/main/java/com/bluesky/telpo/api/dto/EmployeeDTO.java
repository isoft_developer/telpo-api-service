package com.bluesky.telpo.api.dto;


import lombok.*;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@Builder
@ToString
public class EmployeeDTO {
    private Long employeeId;
    private String employeeNo;
}
