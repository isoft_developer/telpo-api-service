package com.bluesky.telpo.api.memory;

import com.bluesky.telpo.api.dto.EmployeeDTO;
import com.bluesky.telpo.api.repository.FaceIdRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.Collection;
import java.util.Map;
import java.util.stream.Collectors;

@Slf4j
@Service
public class EmployeeMemory {

    private final FaceIdRepository faceIdRepository;
    private Map<Long, String> employeeMap;

    @Autowired
    public EmployeeMemory(FaceIdRepository faceIdRepository) {
        this.faceIdRepository = faceIdRepository;
    }

    @EventListener(ApplicationReadyEvent.class)
    public void init() {
        this.refresh();
    }

    public void refresh() {
        Collection<EmployeeDTO> employees = this.faceIdRepository.findAllEmployee();
        if(!CollectionUtils.isEmpty(employees)) {
            this.employeeMap = employees.stream().collect(Collectors.toMap(EmployeeDTO::getEmployeeId, EmployeeDTO::getEmployeeNo));
            log.info("Current employee stored in memory {}", this.employeeMap);
        }
    }

    public Map<Long, String> findAll(){
        return this.employeeMap;
    }
}
