package com.bluesky.telpo.api.service;

import com.bluesky.telpo.api.dto.AttendanceDTO;
import com.bluesky.telpo.api.dto.EmployeeAttendanceDTO;
import com.bluesky.telpo.api.memory.EmployeeMemory;
import com.bluesky.telpo.api.repository.FaceIdRepository;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Service
public class AttendanceServiceImpl implements AttendanceService {

    @Autowired
    private FaceIdRepository faceIdRepository;

    @Autowired
    private EmployeeMemory employeeMemory;

    private final DateTimeFormatter localDateTimeFormatter = DateTimeFormatter.ofPattern("yyyy-MM-dd hh:mm:ss a", Locale.ENGLISH);

    @Override
    public List<EmployeeAttendanceDTO> findAllEmployeeDTR(Boolean isRemoveSeconds) {
        log.info("Find all employee DTR's");
        List<EmployeeAttendanceDTO> employeeAttendanceDTOList = new ArrayList<>();
        Collection<AttendanceDTO> attendanceList = this.faceIdRepository.findAllDTR();

        Map<Long, String> employeeMap = this.employeeMemory.findAll();
        log.info("Current employee list {}", employeeMap);
        log.info("Found DTR's {}", attendanceList.size());
        if (CollectionUtils.isEmpty(attendanceList)) {
            return Collections.emptyList();
        }


        attendanceList.stream().forEach(a -> {
            List<String> attTimeList = Arrays.asList(a.getAttTime().split(","));
            LocalDate localDate = new java.sql.Date(a.getAttDate().getTime()).toLocalDate();

            //
            List<LocalTime> sortedAttList = new ArrayList<>();
            List<String> strEmployeeAttDateTimeList = new ArrayList<>();
            List<LocalDateTime> ldtAttDateTimeList = new ArrayList<>();

            //remove seconds
            if (Objects.equals(isRemoveSeconds, Boolean.TRUE)) {
                attTimeList.stream().forEach(time -> {
                    sortedAttList.add(LocalTime.parse(time).truncatedTo(ChronoUnit.MINUTES));
                });
            } else {
                attTimeList.stream().forEach(time -> {
                    sortedAttList.add(LocalTime.parse(time));
                });
            }

            //get all attendance
            sortedAttList.stream().distinct().forEach(time -> {
                String strLocalDateTime = LocalDateTime.of(localDate, time).format(localDateTimeFormatter);
                strEmployeeAttDateTimeList.add(strLocalDateTime);
                ldtAttDateTimeList.add(LocalDateTime.of(localDate, time));
            });

            //
            employeeAttendanceDTOList.add(EmployeeAttendanceDTO.builder()
                    .employeeNo(employeeMap.get(a.getEmployeeId()))
                    .stringFormatEmployeeAtt(strEmployeeAttDateTimeList)
                    .localDateTimeFormatEmployeeAtt(ldtAttDateTimeList)
                    .build());
        });
        return employeeAttendanceDTOList;
    }

    @Override
    public List<EmployeeAttendanceDTO> findAllEmployeeDTRByWorkNumber(String workNumber) {
        log.info("Find all employee DTR's by work number");
        Map<Long, String> employeeMap = this.employeeMemory.findAll();
        //
        Set<Long> memberIdSet = employeeMap.entrySet().stream().filter(a -> StringUtils.equalsIgnoreCase(a.getValue(), workNumber)).map(Map.Entry::getKey)
                .collect(Collectors.toSet());

        if (CollectionUtils.isEmpty(memberIdSet) || memberIdSet.isEmpty()) {
            log.error("No employee found for member id: {}", workNumber);
            return Collections.emptyList();
        }

        List<EmployeeAttendanceDTO> employeeAttendanceDTOList = new ArrayList<>();
        Long memberId = memberIdSet.stream().findAny().orElse(0L);
        Collection<AttendanceDTO> attendanceList = this.faceIdRepository.findAllDTRBMemberId(memberId);
        //
        log.info("Current employee list {}", employeeMap);
        log.info("Found DTR's {}", attendanceList.size());
        if (CollectionUtils.isEmpty(attendanceList)) {
            return Collections.emptyList();
        }

        attendanceList.stream().filter(f -> Objects.equals(employeeMap.get(f.getEmployeeId()), workNumber)).forEach(a -> {
            List<String> attTimeList = Arrays.asList(a.getAttTime().split(","));
            LocalDate localDate = new java.sql.Date(a.getAttDate().getTime()).toLocalDate();

            //
            List<LocalTime> sortedAttList = new ArrayList<>();
            List<String> strEmployeeAttDateTimeList = new ArrayList<>();
            List<LocalDateTime> ldtAttDateTimeList = new ArrayList<>();

            attTimeList.stream().forEach(time -> {
                sortedAttList.add(LocalTime.parse(time));
            });

            //get all attendance
            sortedAttList.stream().distinct().forEach(time -> {
                String strLocalDateTime = LocalDateTime.of(localDate, time).format(localDateTimeFormatter);
                strEmployeeAttDateTimeList.add(strLocalDateTime);
                ldtAttDateTimeList.add(LocalDateTime.of(localDate, time));
            });

            //
            employeeAttendanceDTOList.add(EmployeeAttendanceDTO.builder()
                    .employeeNo(employeeMap.get(a.getEmployeeId()))
                    .stringFormatEmployeeAtt(strEmployeeAttDateTimeList)
                    .localDateTimeFormatEmployeeAtt(ldtAttDateTimeList)
                    .build());
        });
        return employeeAttendanceDTOList;
    }

    @Override
    public void refreshEmployee() {
        log.info("Reloading employee data");
        this.employeeMemory.refresh();
    }
}
