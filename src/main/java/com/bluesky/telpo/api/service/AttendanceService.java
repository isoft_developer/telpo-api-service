package com.bluesky.telpo.api.service;

import com.bluesky.telpo.api.dto.EmployeeAttendanceDTO;

import java.util.List;

public interface AttendanceService {
    List<EmployeeAttendanceDTO> findAllEmployeeDTR(Boolean isRemoveSeconds);
    List<EmployeeAttendanceDTO> findAllEmployeeDTRByWorkNumber(String workNumber);
    void refreshEmployee();
}
