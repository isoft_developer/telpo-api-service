package com.bluesky.telpo.api.controller;

import com.bluesky.telpo.api.dto.EmployeeAttendanceDTO;
import com.bluesky.telpo.api.service.AttendanceService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@RestController
@RequestMapping("/api/v1.0.0/attendance")
public class AttendanceController {

    @Autowired
    private AttendanceService attendanceService;

    @GetMapping(path = "/all", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EmployeeAttendanceDTO> findAll(@RequestParam(value = "isRemoveSeconds", required = false) Boolean isRemoveSeconds) {
        return this.attendanceService.findAllEmployeeDTR(isRemoveSeconds);
    }

    @GetMapping(path = "/employee/{workNumber}", produces = MediaType.APPLICATION_JSON_VALUE)
    public List<EmployeeAttendanceDTO> findAllByWorkNumber(@PathVariable("workNumber") String workNumber) {
        return this.attendanceService.findAllEmployeeDTRByWorkNumber(workNumber);
    }

    @GetMapping(path = "/refresh-employee-data", produces = MediaType.APPLICATION_JSON_VALUE)
    public String refreshEmployeeData() {
        this.attendanceService.refreshEmployee();
        return "Employee details successfully refreshed. All newly added employee are now ready for dtr processing.";
    }
}
