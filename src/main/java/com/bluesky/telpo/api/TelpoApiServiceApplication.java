package com.bluesky.telpo.api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TelpoApiServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(TelpoApiServiceApplication.class, args);
	}

}
