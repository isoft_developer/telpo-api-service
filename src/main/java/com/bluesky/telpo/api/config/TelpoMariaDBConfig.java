package com.bluesky.telpo.api.config;

import com.bluesky.telpo.api.component.TelpoComponent;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;

import javax.sql.DataSource;

@Configuration
public class TelpoMariaDBConfig {

    @Bean(name = "telpoTemplate")
    public NamedParameterJdbcTemplate telpoTemplate(@Autowired @Qualifier("telpoMariaDatabase") DataSource telpoMariaDatabase) {
        return new NamedParameterJdbcTemplate(telpoMariaDatabase);
    }

    @Bean(name = "telpoMariaDatabase")
    public DataSource telpoMariaDatabase(@Autowired TelpoComponent properties) {
        HikariConfig config = new HikariConfig();
        config.setJdbcUrl(properties.getUrl());
        config.setUsername(properties.getUsername());
        config.setPassword(properties.getPassword());
        config.setPoolName("TelpoBlueskyApi");
        //
        config.setMinimumIdle(5);
        config.setMaximumPoolSize(20);
        config.setIdleTimeout(15 * 60 * 1000L);
        config.setConnectionTimeout(5000L);
        config.setAutoCommit(false);
        //
        config.addDataSourceProperty("cachePrepStmts", "true");
        config.addDataSourceProperty("prepStmtCacheSize", "250");
        config.addDataSourceProperty("useSSL", "false");
        config.addDataSourceProperty("verifyServerCertificate", "false");
        config.addDataSourceProperty("useCursorFetch", "true");
        config.addDataSourceProperty("useDynamicCharsetInfo", "false");
        config.addDataSourceProperty("useCompression", "true");
        //
        return new HikariDataSource(config);
    }
}
