package com.bluesky.telpo.api.mapper;

import com.bluesky.telpo.api.dto.AttendanceDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class AttendanceMapper implements RowMapper<AttendanceDTO> {
    @Override
    public AttendanceDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return AttendanceDTO.builder()
                .employeeId(rs.getLong("employeeId"))
                .attDate(rs.getDate("attDate"))
                .attTime(rs.getString("attTime"))
                .build();
    }
}
