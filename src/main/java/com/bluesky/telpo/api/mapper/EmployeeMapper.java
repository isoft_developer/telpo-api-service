package com.bluesky.telpo.api.mapper;

import com.bluesky.telpo.api.dto.EmployeeDTO;
import org.springframework.jdbc.core.RowMapper;

import java.sql.ResultSet;
import java.sql.SQLException;

public class EmployeeMapper implements RowMapper<EmployeeDTO> {
    @Override
    public EmployeeDTO mapRow(ResultSet rs, int rowNum) throws SQLException {
        return EmployeeDTO.builder()
                .employeeId(rs.getLong("employeeId"))
                .employeeNo(rs.getString("employeeNo"))
                .build();
    }
}
