package com.bluesky.telpo.api.repository;

import com.bluesky.telpo.api.dto.AttendanceDTO;
import com.bluesky.telpo.api.dto.EmployeeDTO;
import com.bluesky.telpo.api.mapper.AttendanceMapper;
import com.bluesky.telpo.api.mapper.EmployeeMapper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Repository
@Slf4j
public class FaceIdRepository {

    @Autowired
    @Qualifier("telpoTemplate")
    private NamedParameterJdbcTemplate telpoTemplate;

    private static final String findAllDTR = "select a.member_id as employeeId, a.at_records as attTime, a.at_date attDate from zh_attendance_logs a ";

    private static final String findAllDTRByMemberId = "select a.member_id as employeeId, a.at_records as attTime, a.at_date attDate from zh_attendance_logs a where a.member_id =:memberId ";

    private static final String findAllEmployee = "select m.id as employeeId, m.mobile as employeeNo from zh_member m where m.mobile <> '' ";


    @Transactional(readOnly = true)
    public Collection<EmployeeDTO> findAllEmployee() {
        log.debug("Querying all employee");
        return telpoTemplate.query(findAllEmployee, new EmployeeMapper());
    }

    @Transactional(readOnly = true)
    public Collection<AttendanceDTO> findAllDTR() {
        log.debug("Querying all dtr from telpo db");
        return telpoTemplate.query(findAllDTR, new AttendanceMapper());
    }

    @Transactional(readOnly = true)
    public Collection<AttendanceDTO> findAllDTRBMemberId(Long memberId) {
        log.debug("Querying all dtr by member id from telpo db");
        Map<String, Long> params = new HashMap<>();
        params.put("memberId", memberId);
        return telpoTemplate.query(findAllDTRByMemberId, params, new AttendanceMapper());
    }
}
